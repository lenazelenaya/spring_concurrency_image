package bsa.java.concurrency.exceptions;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.Map;

@Slf4j
@ControllerAdvice
public class HashExceptionHandler {
    @ExceptionHandler(NullPointerException.class)
    public ResponseEntity<?> handleNullPointerException(NullPointerException ex) {
        log.error("NullPointerException: " + ex.getMessage());
        return new ResponseEntity<>(Map.of("message", "Id is not correct"), HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(HashException.class)
    public ResponseEntity<?> handleHashException(HashException ex) {
        log.error("Error in hash function: " + ex.getMessage());
        return new ResponseEntity<>(Map.of("message", "Error in hash function"), HttpStatus.FORBIDDEN);
    }
}
