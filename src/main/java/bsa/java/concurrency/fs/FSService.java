package bsa.java.concurrency.fs;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Service
public class FSService implements FileSystem{

    private final String path = "C:\\images\\";

    @Override
    public CompletableFuture<String> saveFile(MultipartFile file) {
        ExecutorService executor = Executors.newFixedThreadPool(5);
        return CompletableFuture.supplyAsync(() -> saveImg(file), executor);
    }

    public String saveImg(MultipartFile file){
        var newPath = Paths.get(path + file.getOriginalFilename());
        byte[] bytes;
        String path = null;
        try {
            bytes = file.getBytes();
            path = Files.write(newPath, bytes).toString();
        } catch (IOException exception) {
            exception.getMessage();
        }
        return path;
    }

    @Override
    public void deleteAllFiles() {
        File dirWithImages = new File(path);
        File[] files = dirWithImages.listFiles();
        if(files.length != 0)
        for(File file : files) {
            if(!file.delete()) {
                System.out.println("File is not deleted: " + file.getAbsolutePath());
            }
        }
    }

    @Override
    public void deleteByURL(String url){
        var file = new File(url);
        if(!file.delete()) {
            System.out.println("File is not deleted: " + file.getAbsolutePath());
        }
    }

    public byte[] getBytes(MultipartFile file){
        byte[] bytes = null;
        try {
            bytes = file.getBytes();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return bytes;
    }
}
