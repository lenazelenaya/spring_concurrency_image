package bsa.java.concurrency.image;

import bsa.java.concurrency.exceptions.HashException;
import org.springframework.stereotype.Component;

import javax.imageio.ImageIO;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;

@Component
public class ImageHash {

    public long getHash(byte[] image) {
        try {
            return getDHash(grayscaleImg(
                    ImageIO.read(new ByteArrayInputStream(image))
            ));
        } catch (Exception ex) {
            throw new HashException(ex);
        }
    }

    private static BufferedImage grayscaleImg(BufferedImage bufferedImage) {
        var resized = bufferedImage.getScaledInstance(9, 9, Image.SCALE_SMOOTH);
        var grayScaled = new BufferedImage(9, 9, BufferedImage.TYPE_BYTE_GRAY);
        grayScaled.getGraphics()
                .drawImage(resized, 0, 0, null);
        return grayScaled;
    }

    public static long getDHash(BufferedImage bufferedImage) {
        long hash = 0;
        for (var i = 1; i < 9; i++) {
            for (var j = 1; j < 9; j++) {
                var first = bufferedImage.getRGB(j - 1, i - 1) & 0b11111111;
                var second = bufferedImage.getRGB(j, i) & 0b11111111;
                hash |= second > first ? 1 : 0;
                hash <<= 1;
            }
        }

        return hash;
    }
}
