package bsa.java.concurrency.image;

import bsa.java.concurrency.image.dto.SearchResultDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface ImageRepository extends JpaRepository<Image, UUID> {
    @Query(value = "select cast(id as varchar(255)) as imageId, " +
            "getMatchPercent(hash, ?1) * 100 as matchPercent, " +
            "url as imageUrl from images" +
            " where getMatchPercent(hash, ?1) >= ?2", nativeQuery = true)
    List<SearchResultDTO> getImages(long hash, double threshold);
}
