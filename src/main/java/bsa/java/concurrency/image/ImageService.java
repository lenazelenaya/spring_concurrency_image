package bsa.java.concurrency.image;

import bsa.java.concurrency.fs.FSService;
import bsa.java.concurrency.image.dto.SearchResultDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

@Service
public class ImageService {
    @Autowired
    private ImageRepository imageRepository;

    @Autowired
    private FSService fsService;

    @Autowired
    private ImageHash imageHash;

    public CompletableFuture<Void> saveAll(MultipartFile[] files){
        var futures = Arrays.stream(files)
                .parallel()
                .map(file -> {
                    var future = fsService.saveFile(file);
                    var hash = imageHash.getHash(fsService.getBytes(file));
                    future.thenAccept(
                            saved -> imageRepository.save(new Image(UUID.randomUUID(), saved, hash))
                    );
                    return future;
                }).toArray(CompletableFuture[]::new);
        return CompletableFuture.allOf(futures);
    }

    public void deleteAll(){
        fsService.deleteAllFiles();
        imageRepository.deleteAll();
    }

    public void deleteById(UUID id) {
        String imageURL = imageRepository.findById(id).map(Image::getUrl).get();
        imageRepository.deleteById(id);
        fsService.deleteByURL(imageURL);
    }

    public List<SearchResultDTO> search(MultipartFile file, double threshold){
        var hash = imageHash.getHash(fsService.getBytes(file));
        var repositoryImages = imageRepository.getImages(hash, threshold);

        if(repositoryImages.size() == 0) {
            CompletableFuture.supplyAsync(() -> {
                var future = fsService.saveFile(file);
                future.thenAccept(result ->
                        imageRepository.save(new Image(UUID.randomUUID(), result, hash)));
                return future;
            });
        }
        return repositoryImages;
    }

}
