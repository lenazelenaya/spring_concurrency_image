create or replace function getMatchPercent(IN hash1 bigint, IN hash2 bigint)
    returns real as $$
declare
    xor bigint := hash1 # hash2;
    count bigint := 0;
    difference real := 0.0;
begin
    if (xor < 0) then
        xor := ~xor;
    end if;

    while(xor > 0)
        loop
            count := count + (xor & 1);
            xor := xor >> 1;
        end loop;

    difference := 1 - cast(count as double precision)/64;
    return difference;
end; $$
    language plpgsql;